import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Project from '@/components/Project'
import Contact from '@/components/Contact'
import Shop from '@/components/Shop'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/project/:project_slug',
      name: 'project',
      component: Project
    },
    {
      path: '/contact',
      name: 'contact',
      component: Contact
    },
    {
      path: '/shop',
      name: 'shop',
      component: Shop
    }
  ],
  scrollBehavior (to, from, savedPosition) {
    if (to.name.startsWith('project')) {
      return { x: 0, y: 0 }
    }
  }
})
